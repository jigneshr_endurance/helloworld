### WORKING ######
###### 0. Multi Stage docker builds with external sonar scanner and open JDK ALPINE ######
FROM maven as build
ADD . /usr/src/myapp
WORKDIR /usr/src/myapp
RUN mvn -s settings-nexus.xml clean package

# SonarScanner Stage , Upload the coverage and test reports to the sonar server
FROM eigdevstack/sonar-scanner
COPY --from=build /usr/src/myapp /usr/src/myapp
WORKDIR /usr/src/myapp
RUN sonar-scanner -Dsonar.host.url=https://sonarqube.dstack.tec

FROM openjdk:8-jre-alpine
MAINTAINER  Author Name jignesh.r@endurance.com
VOLUME /tmp
COPY --from=build /usr/src/myapp/target/helloworld-0.0.1-SNAPSHOT.jar app.jar
RUN sh -c 'touch /app.jar'
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
###### 0. Multi Stage docker build with external sonar scanner and open JDK ALPINE ######
### WORKING ######










### NOT WORKING ######
###### 0. Multi Stage docker build user maven wrapper for build and then RUN image based onopen JDK ALPINE ######
#FROM alpine:3.7 as build
#
#ADD . /usr/src/myapp/
#WORKDIR /usr/src/myapp
#RUN sh -c 'chmod +x mvnw'
#RUN mvnw.cmd clean package -DskipTests
#
#FROM openjdk:8-jre-alpine
#
#MAINTAINER  Author Name jignesh.r@endurance.com
#VOLUME /tmp
#COPY --from=build /usr/src/myapp/target/helloworld-0.0.1-SNAPSHOT.jar app.jar
#RUN sh -c 'touch /app.jar'
#EXPOSE 8080
#ENTRYPOINT ["java","-jar","app.jar"]
###### 0. Multi Stage docker build with open JDK ALPINE ######
### NOT WORKING ######



### WORKING ######
###### 0. Multi Stage docker build with open JDK ALPINE ######
#FROM maven as build
#ADD . /usr/src/myapp
#WORKDIR /usr/src/myapp
#RUN mvn -s settings-nexus.xml clean package -DskipTests
#
#FROM openjdk:8-jre-alpine
#MAINTAINER  Author Name jignesh.r@endurance.com
#VOLUME /tmp
#COPY --from=build /usr/src/myapp/target/helloworld-0.0.1-SNAPSHOT.jar app.jar
#RUN sh -c 'touch /app.jar'
#EXPOSE 8080
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
###### 0. Multi Stage docker build with open JDK ALPINE ######
### WORKING ######



##### 1. Multi Stage docker build with oracle JDK######
#FROM maven as build
#ADD . /usr/src/myapp
#WORKDIR /usr/src/myapp
#RUN mvn -s settings.xml clean package -DskipTests
#
#
#FROM frolvlad/alpine-oraclejdk8:slim
#MAINTAINER  Author Name jignesh.r@endurance.com
#VOLUME /tmp
#COPY --from=build /usr/src/myapp/target/helloworld-0.0.1-SNAPSHOT.jar app.jar
#RUN sh -c 'touch /app.jar'
#EXPOSE 8080
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
##### 1. Multi Stage docker build with oracle JDK ######


##### 2. Only RUN image , build on local machine  ######
#FROM frolvlad/alpine-oraclejdk8:slim
#VOLUME /tmp
#ADD target/helloworld-0.0.1-SNAPSHOT.jar app.jar
#RUN sh -c 'touch /app.jar'
#EXPOSE 8080
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
##### 2. Only RUN image,  build on local machine  ######


##### 3. Only RUN image build on local machine-- OPEN JRE  SLIM######
#FROM openjdk:8-jre-slim
#VOLUME /tmp
#ADD target/helloworld-0.0.1-SNAPSHOT.jar app.jar
#RUN sh -c 'touch /app.jar'
#EXPOSE 8080
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
##### 3. Only RUN image build on lcal machine - OPEN JRE SLIM ######



##### 4. Only RUN image build on local machine-- OPEN JDK  SLIM######
#FROM openjdk:8-jdk-slim
#VOLUME /tmp
#ADD target/helloworld-0.0.1-SNAPSHOT.jar app.jar
#RUN sh -c 'touch /app.jar'
#EXPOSE 8080
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
##### 4. Only RUN image build on lcal machine - OPEN JDK SLIM ######

##### 5. Only RUN image build on local machine-- OPEN JRE ALPINE  ######
#FROM openjdk:8-jre-alpine
#VOLUME /tmp
#ADD target/helloworld-0.0.1-SNAPSHOT.jar app.jar
#RUN sh -c 'touch /app.jar'
#EXPOSE 8080
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
##### 5. Only RUN image build on lcal machine - OPEN JRE ALPINE ######


##### 6. Only RUN image build on local machine-- OPEN JDK ALPINE  ######
#FROM openjdk:8-jdk-alpine
#VOLUME /tmp
#ADD target/helloworld-0.0.1-SNAPSHOT.jar app.jar
#RUN sh -c 'touch /app.jar'
#EXPOSE 8080
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
##### 6. Only RUN image build on lcal machine - OPEN JDK ALPINE ######
