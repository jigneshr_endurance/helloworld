package com.tbox;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by jignesh.r on 2/25/2017.
 */

@Data
@NoArgsConstructor(force = true)
public class ResponseWrapper<T> {
    T response;

    public ResponseWrapper(T response) {
        this.response = response;
    }

}
