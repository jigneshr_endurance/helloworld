package com.tbox;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloworldApplicationTests {

	@Test
	public void contextLoads() {
	}


	private HellowWorldController hellowWorldController;
	private HelloWorldService hellowWorldService;

	@Before
	public void setUp() {
		hellowWorldController = new HellowWorldController();//Mockito.mock(HellowWorldController.class);
		hellowWorldService = new HelloWorldService(); //Mockito.mock(HelloWorldService.class);

	}


	@Test
	public void sayHello()
	{
		ResponseEntity<ResponseWrapper<String>> responseWrapper= hellowWorldController.sayHello("AAA");
	}
	@Test
	public void sayHello1()
	{
		ResponseEntity<ResponseWrapper<String>> responseWrapper = hellowWorldController.sayHelloRequestBody("AAA");
	}
	@Test
	public void sayHello2()
	{
		ResponseEntity<ResponseWrapper<String>> responseWrapper = hellowWorldController.sayHelloRequestBody3(new RequestWrapper<>("AAA"));
	}

	@Test
	public void upload() {
		ResponseEntity<ResponseWrapper<String>> responseWrapper = hellowWorldController.upload(new MockMultipartFile("test.txt", "123".getBytes()));
	}

	@Test
	public void hello123()
	{
		hellowWorldController.hello1();
	}

	@Test
	public void hello11() {
		hellowWorldService.hello11();
	}

}